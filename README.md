Sync file server and client

Requires:

1) rabbitmq

2) perl modules:

* Fcntl
* FindBin
* IO::Socket::INET
* JSON::XS
* MIME::Base64
* AnyEvent
* AnyEvent::Handle
* AnyEvent::RabbitMQ
* Linux::Inotify2
* Net::Server
* Net::Server::Fork
* Net::AMQP::RabbitMQ

Server:
```
cd <homedir>
sh bin/start-server.sh
```

Client:
```
cd <homedir>
perl bin/client.pl <filename>
```
