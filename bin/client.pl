#!/usr/bin/perl

use strict;

use FindBin qw($Bin);
use lib "${Bin}/../lib";

use SyncFile::Client;

my $client = SyncFile::Client->new(
    'filename' => $ARGV[0],
    'host' => 'localhost',
    'port' => 9090
)
or die "Cannot init client";

$client->run(
    'sleep' => 10
);

1;
