#!/usr/bin/perl

use strict;

use FindBin qw($Bin);
use lib "${Bin}/../lib";

use SyncFile::Server;

my ($filename, $clientStatDir, $logDir) = @ARGV;

die "Invalid filename" if !-e $filename;
die "Invalid stat dir" if !-d $clientStatDir;
die "Invalid log dir" if !-d $logDir;

initStatDir($clientStatDir);

my $server = SyncFile::Server->new(
    'port' => '9090',
    'ipv' => '*',
    'sf_filename' => $filename,
    'sf_statdir' => $clientStatDir,
    'sf_logdir' => $logDir
);

$server->run();

# Remove obsoleted stat files
sub initStatDir
{
    my ($dir) = @_;

    opendir(STATDIR, $dir)
        or die "Cannot open dir: $!";

    my @statFiles = grep { !/^\.\.?$/ } readdir(STATDIR);

    if (@statFiles)
    {
        unlink (map { "${dir}/${_}" } @statFiles)
            or die "Cannot unlink: $!";
    }

    closedir(STATDIR)
        or die "Cannot close dir: $!";
}
