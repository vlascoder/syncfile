#!/bin/sh

HOMEDIR=~/syncfile
BINDIR=${HOMEDIR}/bin
LOGDIR=${HOMEDIR}/log
FILENAME=${HOMEDIR}/my.file

if [ ! -f ${FILENAME} ]
then
    echo Invalid filename: ${FILENAME}
    exit
fi

# Restart file inspector
INSPECTOR_PID=`ps -eFww | grep inspector.pl | grep -v grep | awk '{print $2}'`
[ -n "${INSPECTOR_PID}" ] && kill ${INSPECTOR_PID}
perl ${BINDIR}/inspector.pl ${FILENAME} >>${LOGDIR}/inspector.log 2>>${LOGDIR}/inspector.err &

# Run server (interactive mode)
perl ${BINDIR}/server.pl ${FILENAME} ${HOMEDIR}/stat ${HOMEDIR}/log
