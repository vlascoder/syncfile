#!/usr/bin/perl

use strict;

use Linux::Inotify2;
# TODO {
# use File::ChangeNotify::Watcher
# }

use Net::AMQP::RabbitMQ;
use JSON::XS;

STDERR->autoflush(1);

my ($filename) = @ARGV;
# TODO: get filename using ARGV or GetOpt::Long

# Init {
(-e $filename)
    or die "No file to watch: $filename";

my $mq = Net::AMQP::RabbitMQ->new()
    or die "Cannot create mq object: $!";

my $inotify = Linux::Inotify2->new()
    or die "Cannot create inotify object: $!";

my $json = JSON::XS->new()
    or die "Cannot create json object: $!";
# }

$mq->connect('localhost', { user => 'guest', password => 'guest' });
$mq->channel_open(1);

while (1)
{
    $inotify->watch(
        $filename,
        IN_MODIFY,
        sub {
            my ($e) = @_;

            my $name = $e->fullname;
            my ($size, $modified) = (stat($name))[7,9];

            my $payload = {
                'filename' => $name,
                'size' => $size,
                'modified' => $modified,
                'ts' => time()
            };

            my $payloadJSON = $json->encode($payload);

            $mq->exchange_declare(1, 'myfile', {
                'auto_delete' => 0,
                'exchange_type' => 'fanout'
            });
            # $mq->purge(1, 'myfile');
            $mq->publish(1, 'myfile', $payloadJSON, {
                'exchange' => 'myfile'
            });

            $e->w->cancel;
        }
    );

    $inotify->poll;
}
