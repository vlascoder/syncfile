package SyncFile::Server;

use strict;
use base qw(Net::Server::Fork);

use AnyEvent;
use AnyEvent::RabbitMQ;
use Fcntl qw(:flock);
use JSON::XS;
use MIME::Base64;

my $maxFileChunk = 1024;

my $json = JSON::XS->new()
    or die "Cannot create JSON object";

sub process_request
{
    my ($self) = @_;

    my $filename = $self->get_property('sf_filename');
    my $clientStatDir = $self->get_property('sf_statdir');
    my $logFilename = join('/', $self->get_property('sf_logdir'), 'server.log');

    # Open log file
    open LOG, ">>", $logFilename
        or die "Cannot open file: $!";

    flock(LOG, LOCK_SH)
        or die "Cannot lock file: $!";

    LOG->autoflush(1);

    # Open stat file
    if (-d $clientStatDir)
    {
        open STAT, ">${clientStatDir}/$$"
            or die "Cannot open stat file: $!";

        flock(STAT, LOCK_SH)
            or die "Cannot lock file: $!";

        STAT->autoflush(1);
    }

    # Init AnyEvent loop
    my $cv = AnyEvent->condvar;

    # Rabbit watcher
    my $rabbitWatcher = AnyEvent::RabbitMQ->new->load_xml_spec()->connect(
        'host' => 'localhost',
        'port' => 5672,
        'user' => 'guest',
        'pass' => 'guest',
        'vhost' => '/',
        'timeout' => 1,
        'tls' => 0,
        'on_success' => sub {
            my ($w) = @_;

            $w->open_channel(
                'on_success' => sub {
                    my ($channel) = @_;

                    my $queueName = "myfile.$$";
                    my $exchangeName = 'myfile';

                    $channel->declare_exchange(
                        'exchange' => $exchangeName,
                        'type' => 'fanout',
                        'auto_delete' => 0,
                        'on_failure' => $cv,
                        'on_success' => sub {
                            $channel->declare_queue(
                                'queue' => $queueName,
                                'auto_delete' => 1,
                                'on_failure' => $cv,
                                'on_success' => sub {
                                    $channel->bind_queue(
                                        'queue' => $queueName,
                                        'exchange' => $exchangeName,
                                        'routing_key' => 'myfile'
                                    );
                                    $channel->consume(
                                        'queue' => $queueName,
                                        'on_failure' => $cv,
                                        'on_consume' => sub {
                                            my ($message) = @_;
                                            print LOG "[$$] Message received: ". $message->{'body'}->payload ."\n";
                                            print STDOUT $message->{'body'}->payload . "\n";
                                        }
                                    );
                                }
                            );
                        }
                    );
                },
                'on_failure' => $cv
            );
        },
        'on_failure' => $cv
    );

    # Input handler
    my $lastInputTS = time;
    my $inputHandler;

    $inputHandler = AnyEvent::Handle->new(
        'fh' => \*STDIN,
        'on_error' => sub {
            my ($handler, $fatal, $message) = @_;
            print LOG "[$$] Input handler error: $message\n";
            $handler->destroy();
            $cv->send();
        },
        'on_read' => sub {
            $inputHandler->push_read(
                'line' => sub {
                    my ($handler, $line) = @_;

                    $lastInputTS = time;

                    $line =~ s/[\r\n]+$//gs;
                    print LOG "[$$] Incoming data: $line\n";

                    if ($line =~ /^\{.*?\}$/)
                    {
                        my $data = $json->decode($line);

                        if ($data)
                        {
                            if (exists $data->{'method'})
                            {
                                my ($size, $modified) = (stat($filename))[7,9];

                                if ($data->{'method'} eq 'stat')
                                {
                                    sendPayload({
                                        'size' => $size,
                                        'modified' => $modified
                                    });
                                }

                                elsif ($data->{'method'} eq 'pong')
                                {
                                    if (exists $data->{'filesize'})
                                    {
                                        seek(STAT, 0, 0)
                                            or die "Cannot seek file: $!";

                                        print STAT join('',
                                            ('0') x (20 - length($data->{'filesize'})),
                                            $data->{'filesize'}
                                        ), "\n";
                                    }
                                }

                                elsif ($data->{'method'} eq 'get')
                                {
                                    if ($data->{'offset'} < $size)
                                    {
                                        open(FILE, $filename)
                                            or die "Cannot open file $filename: $!";

                                        binmode(FILE);

                                        my $buff;

                                        if (
                                            $data->{'offset'}
                                            && ($data->{'offset'} < $size)
                                        ){
                                            seek(FILE, $data->{'offset'}, 0)
                                                or die "Cannot seek file $filename to ". $data->{'offset'} .": $!";
                                        }

                                        my $readSize = $size - $data->{'offset'};
                                        $readSize = $maxFileChunk if $readSize > $maxFileChunk;

                                        read(FILE, $buff, $readSize)
                                            or die "Cannot read file $filename: $!";

                                        close(FILE)
                                            or die "Cannot close file $filename: $!";

                                        if ($buff)
                                        {
                                            sendPayload({
                                                'method' => 'chunk',
                                                'chunk' => encode_base64($buff, '')
                                            });
                                        }
                                    }
                                }

                                elsif ($data->{'method'} eq 'get-client-stat')
                                {
                                    my %clients = ();

                                    if (-d $clientStatDir)
                                    {
                                        opendir STATDIR, $clientStatDir
                                            or die "Cannot open stat dir: $!";

                                        STATFILE:
                                        while (my $statFile = readdir(STATDIR))
                                        {
                                            next STATFILE if $statFile =~ /^\.\.?$/;

                                            open(CLIENTSTAT, "${clientStatDir}/${statFile}")
                                                or die "Cannot open file: $!";

                                            my ($size) = <CLIENTSTAT>;
                                            $size =~ s/\D+//gs if $size;

                                            print LOG "[$$] Reading stat file ${statFile}: ${size}\n";

                                            close(CLIENTSTAT);

                                            if ($size && $size =~ /^0*(\d+)/)
                                            {
                                                $clients{ $statFile } = $1;
                                            }
                                        }

                                        closedir(STATDIR);
                                    }

                                    sendPayload({
                                        'clients' => \%clients
                                    });

                                    undef %clients;
                                }
                            }
                        }
                    }
                }
            );
        }
    );

    # Timeout
    my $lastInputWatcher = AnyEvent->timer(
        'interval' => 10,
        'cb' => sub {
            if ((time - $lastInputTS) > 30)
            {
                print LOG "[$$] Close by timeout\n";
                $cv->send();
            }
        }
    );

    # Ping-pong
    my $pingTimer = AnyEvent->timer(
        'interval' => 10,
        'cb' => sub {
            sendPayload({
                'method' => 'ping'
            });
        }
    );

    $cv->recv();

    undef $rabbitWatcher;

    $inputHandler->destroy();
    undef $inputHandler;

    undef $lastInputWatcher;

    close(STAT);

    if (-e "${clientStatDir}/$$")
    {
        unlink("${clientStatDir}/$$")
            or die "Cannot unlink file: $!";
    }

    close(LOG);
}

sub sendPayload
{
    my ($data) = @_;

    my $payload = $json->encode($data);

    print LOG "[$$] Send payload: ${payload}\n";
    print STDOUT "${payload}\n";
}

1;
