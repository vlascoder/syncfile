package SyncFile::Client;

use strict;
use IO::Socket::INET;
use JSON::XS;
use MIME::Base64;
use AnyEvent;
use AnyEvent::Handle;

sub new
{
    my ($class, %params) = @_;

    for my $item (qw(filename host port))
    {
        if (!$params{ $item })
        {
            warn "${item} is undefined";
            return undef;
        }
    }

    my $self = {
        'filename' => $params{'filename'},
        'host' => $params{'host'},
        'port' => $params{'port'},
        'sleep' => $params{'sleep'}
    };

    bless $self, $class;

    $self->initFile();

    $self->{'json'} = JSON::XS->new()
        or die "Cannot create JSON object";

    return $self;
}

sub run
{
    my ($self, %params) = @_;

    $self->getFileSize();

    while (!$self->{'socket'})
    {
        # init
        $self->openSocket();

        if ($self->{'socket'})
        {
            $self->{'cv'} = AnyEvent->condvar;
            $self->addSocketHandler();

            $self->sendPayload({
                'method'=>'stat'
            });

            # wait
            $self->{'cv'}->recv();

            # destroy
            $self->removeSocketHandler();
            $self->closeSocket();
        }

        sleep $params{'sleep'} || 10;
    }
}

sub initFile
{
    my ($self) = @_;

    if (! -e $self->{'filename'})
    {
        open(FILE, ">", $self->{'$filename'})
            or die "Cannot open '". $self->{'filename'} ."': $!";

        close(FILE);
    }
}

sub getFileSize
{
    my ($self) = @_;

    # Get replica size
    ($self->{'filesize'}) = (stat($self->{'filename'}))[7];
}

sub openSocket
{
    my ($self) = @_;

    $self->{'socket'} = IO::Socket::INET->new(
        'PeerAddr' => $self->{'host'},
        'PeerPort' => $self->{'port'},
        'Proto' => 'tcp'
    )
    or warn "Cannot create socket: $!";

    $self->{'socket'}->autoflush(1)
        if $self->{'socket'};
}

sub closeSocket
{
    my ($self) = @_;

    $self->{'socket'}->shutdown(2)
        or die "Cannot shutdown socket: $!";

    $self->{'socket'}->close()
        or die "Cannot close socket: $!";

    delete $self->{'socket'};
}

sub addSocketHandler
{
    my ($self) = @_;

    $self->{'socketHandler'} = AnyEvent::Handle->new(
        'fh' => $self->{'socket'},
        'on_error' => sub {
            my ($handler, $fatal, $message) = @_;
            print STDOUT "[$$] Input handler error: $message\n";
            $handler->destroy();
            $self->{'cv'}->send();
        },
        'on_read' => sub {
            $self->{'socketHandler'}->push_read(
                'line' => sub {
                    my ($handler, $line) = @_;

                    $line =~ s/[\r\n]+$//gs;
                    print STDOUT "[$$] Incoming data: $line\n";

                    $self->handleInput(
                        'line' => $line
                    );
                }
            );
        }
    );
}

sub removeSocketHandler
{
    my ($self) = @_;

    if ($self->{'socketHandler'})
    {
        $self->{'socketHandler'}->destroy();
        $self->{'cv'}->send();
        delete $self->{'socketHandler'};
    }
}

sub handleInput
{
    my ($self, %params) = @_;

    my $line = $params{'line'};

    if ($line =~ /^\{.*?\}$/)
    {
        my $data = $self->{'json'}->decode($line);

        if ($data)
        {
            if (exists $data->{'method'})
            {
                if ($data->{'method'} eq 'ping')
                {
                    $self->sendPayload({
                        'method' => 'pong',
                        'filesize' => $self->{'filesize'}
                    });
                }
                elsif ($data->{'method'} eq 'chunk')
                {
                    my $decoded = decode_base64($data->{'chunk'});

                    open(FILE, ">>", $self->{'filename'});
                    binmode(FILE);
                    print FILE $decoded;
                    close(FILE);

                    $self->{'filesize'} += length($decoded);
                }
            }

            if ($data->{'size'} > $self->{'filesize'})
            {
                $self->sendPayload({
                    'method' => 'get',
                    'offset' => $self->{'filesize'}
                });
            }
        }
    }
}

sub sendPayload
{
    my ($self, $data) = @_;

    my $payload = $self->{'json'}->encode($data);

    print "[$$] Send payload: ${payload}\n";
    $self->{'socket'}->print("${payload}\n");
}

1;
